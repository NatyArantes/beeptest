require 'http'
require 'date'
require 'json'

class RatesController < ApplicationController
    def show

        allowed_currencies = ["USD", "EUR", "ARS"]
        rates = []

        if not allowed_currencies.include?(params["id"])
            json_response("currency not allowed", :not_found)
            return
        end

        date_start = (Time.current).to_date
        date_loop = date_start

        7.times do
            url = "http://apilayer.net/api/historical?&access_key=#{ENV['API_KEY']}&currencies=#{params['id']}&source=USD&format=1&date=#{date_loop}"
            response = JSON.parse(HTTP.get(url).body)
            formated_response = {"timestamp" => response['timestamp'], "quotes" => response['quotes']}
            rates = rates.push(formated_response)
            date_loop = date_loop - 1.day
        end
        json_response(rates)

    end
end
