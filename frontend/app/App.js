var React = require('react');
var reactDOM = require('react-dom');

const Page = require('./components/Page');
reactDOM.render(<Page />, document.getElementById('app'));
