const React = require('react');
const Header = require('./Header');
const Content = require('./Content');

class Page extends React.Component{
    constructor (props){
        super(props);
        this.state = {
            currentCurrency: 'EUR'
        }
        this.updateCurrency = this.updateCurrency.bind(this);
    }

    updateCurrency(currency) {
        this.setState({currentCurrency: currency});
    }

    render(){
        console.log(this.state.currentCurrency);
        return (<div>
                <Header onChange={this.updateCurrency}/>
                <Content currency={this.state.currentCurrency}/>

            </div>);
    }
}
module.exports = Page;
