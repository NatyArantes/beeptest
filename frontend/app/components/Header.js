const React = require('react');
require('./Header.css');

class Header extends React.Component{
    render(){
        return (
            <div>
                <h3>CAMBIO</h3>
                <br/><br/>
                <div className="botoes">
                    <button type="button" className="btn btn-default" onClick={() => {this.props.onChange('USD')}}>Dolar</button>
                    <button type="button" className="btn btn-default" onClick={() => {this.props.onChange('ARS')}}>Peso Argentino</button>
                    <button type="button" className="btn btn-default" onClick={() => {this.props.onChange('EUR')}}>Euro</button>
                </div>
            </div>
        );
    }
}
module.exports = Header;
