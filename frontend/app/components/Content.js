const React = require('react');
const Highcharts = require('highcharts');
const HighchartsReact = require('highcharts-react-official').default;
const Api = require('../services/api');

// console.log(Highcharts, HighchartsReact);

class Content extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data:null
        }
    }

    componentDidMount(){
        Api.getRates(this.props.currency)
            .then((data) => {
                this.setState({data: data});
            });
    }

    componentDidUpdate() {
        Api.getRates(this.props.currency)
            .then((data) => {
                this.setState({data: data});
            });
    }

    render(){
        if (this.state.data === null) return null;
        // return (<pre>{JSON.stringify(this.state.data)}</pre>)
        const options = {
            chart: {
                type: 'line'
            },
            title: {
                text: "teste da naty"
            },
            xAxis: {
                categories: this.state.data.timestamp
            },
            series: [
                {
                    data: this.state.data.quotes
                }
            ]
        }
        return (
            <HighchartsReact
                highcharts={Highcharts}
                options={options}
            />
        );
    }
}
module.exports = Content;
