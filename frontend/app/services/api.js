const dateFNS = require('date-fns');

function getRates(currency = 'ARS'){
    const url =`${process.env.API_URL}/rates/${currency}`;
    const request = fetch(url);
    const response = {};
    const promise = request
        .then((data) => {
            return data.json();
        })
        .then((data) => {
            const quotes = [];
            for (var i = 0; i < data.length; i++) {
                quotes[i] = data[i].quotes['USD'+currency];
            }
            response.quotes = quotes;
            return data;
        })
        .then((data) => {
            const timestamp = [];
            for (var j = 0; j < data.length; j++) {
                var dateConverted = new Date (data[j].timestamp * 1000);
                var result = dateFNS.format(
                  dateConverted,
                  'DD/MMM'
                )
                timestamp[j] = result;
            }
            response.timestamp = timestamp;
            return data;
        })
        .then(() => response)
        .catch((error) => {
            console.error(error);
        });

    return promise;
}
module.exports = {getRates};
