# beeptest

This project is built on two main folders. The backend API is developed under beeptest-api, on Ruby on rails. There are two external dependencies on it, being:
- http: for http requisitions on JSON Rates API;
- rack-cors: allows a server to make changes on response's headers.

### Running backend:
Navigate to the project folder

````$ cd beeptest;````

Install dependencies

````$ bundle````

Run the server

````$ API_KEY=<CURRENCY_LAYER_KEY> rails s````

> Obs.: needless to say you should have Ruby and Ruby on rails installed.

## Frontend dependencies
- React and React DOM
- Webpack
- Babel
- Date-fns
- React Highcharts

### Running frontend:
Install dependencies

````$ npm install````

Making the app build

````$ API_URL="<RAILS_API_URL>" npm start````

> By default Rails API starts at http://0.0.0.0:3000

#### Under frontend/public:
Start the web server

````$ python -m SimpleHTTPServer 8000````

> Obs.: you should have node and npm installed on the machine.
